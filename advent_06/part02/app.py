def read_input():
    with open('input.txt') as file:
        return file.read().split('\n\n')

def get_common_party_declarations(input):
    return sorted(set.intersection(*[set(x) for x in input.split()]))

if __name__ == '__main__':
    input = read_input()
    party_common_declaration_counts = [len(get_common_party_declarations(x)) for x in input]
    print(f"Total party common declarations: {sum(party_common_declaration_counts)}")
