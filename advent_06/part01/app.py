def read_input():
    with open('input.txt') as file:
        return file.read().split('\n\n')

def get_party_declarations(input):
    return sorted([x for x in set(input) if not x == '\n'])
                        
if __name__ == '__main__':
    input = read_input()
    party_declaration_counts = [len(get_party_declarations(x)) for x in input]
    print(f"Total items declared: {sum(party_declaration_counts)}")
