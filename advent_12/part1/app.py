from functools import reduce

def read_input():
    with open('input.txt') as file:
        return file.read().strip().split('\n')

forward_fn_lookup = {
    0: 'E',
    1: 'S',
    2: 'W',
    3: 'N'
}

direction_fn_lookup = {
    'N': lambda state, arg: { "Y": state['Y'] + arg },
    'S': lambda state, arg: { "Y": state['Y'] - arg },
    'E': lambda state, arg: { "X": state['X'] + arg },
    'W': lambda state, arg: { "X": state['X'] - arg },
    'F': lambda state, arg: direction_fn_lookup[forward_fn_lookup[state['Rotation'] % 4]](state, arg),
    'L': lambda state, arg: { "Rotation": int(state['Rotation']) - arg / 90 },
    'R': lambda state, arg: { "Rotation": int(state['Rotation']) + arg / 90 },
}

def apply_instruction(old_state, command_string):
    command, argument = command_string[0], int(command_string[1:])
    
    new_state = dict()
    new_state.update(old_state)
    new_state.update(direction_fn_lookup[command](old_state, argument))
    return new_state

if __name__ == '__main__':
    input = read_input()
    original_state = { "X": 0, "Y": 0, "Rotation": 0}
    final_state = reduce(lambda state, command_string: apply_instruction(state, command_string), input, original_state)
    manhattan_distance = abs(final_state['X']) + abs(final_state['Y'])
    print(manhattan_distance)
