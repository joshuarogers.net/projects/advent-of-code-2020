from functools import reduce

def read_input():
    with open('input.txt') as file:
        return file.read().strip().split('\n')

forward_fn_lookup = {
    0: 'E',
    1: 'S',
    2: 'W',
    3: 'N'
}
    
def rotate(state, degrees):
    def rotate_right(state):
        return { 'WaypointX': state['WaypointY'], 'WaypointY': state['WaypointX'] * -1 }
    
    right_turns = int(degrees % 360 / 90)
    return reduce(lambda state, _: rotate_right(state), range(0, right_turns), state)

direction_fn_lookup = {
    'N': lambda state, arg: { 'WaypointY': state['WaypointY'] + arg },
    'S': lambda state, arg: { 'WaypointY': state['WaypointY'] - arg },
    'E': lambda state, arg: { 'WaypointX': state['WaypointX'] + arg },
    'W': lambda state, arg: { 'WaypointX': state['WaypointX'] - arg },
    'F': lambda state, arg: {
        'X': state['X'] + state['WaypointX'] * arg,
        'Y': state['Y'] + state['WaypointY'] * arg
    },
    'L': lambda state, arg: rotate(state, -arg),
    'R': lambda state, arg: rotate(state, arg),
}

def apply_instruction(old_state, command_string):
    command, argument = command_string[0], int(command_string[1:])
    
    new_state = dict()
    new_state.update(old_state)
    new_state.update(direction_fn_lookup[command](old_state, argument))
    return new_state

if __name__ == '__main__':
    input = read_input()
    original_state = { "WaypointX": 10, "WaypointY": 1, "X": 0, "Y": 0 }
    final_state = reduce(lambda state, command_string: apply_instruction(state, command_string), input, original_state)
    manhattan_distance = abs(final_state['X']) + abs(final_state['Y'])
    print(manhattan_distance)
