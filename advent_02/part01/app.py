import re

def read_input():
    with open('input.txt') as file:
        return file.read()

class PasswordEntry:
    def __init__(self, password, policy):
        self.password = password
        self.policy = policy

class PasswordPolicy:
    def __init__(self, letter, min_count, max_count):
        self.letter = letter
        self.min_count = min_count
        self.max_count = max_count

def parse_password_entry(row):
    matches = re.search("^(\d+)-(\d+) (\w): (\w+)$", row)
    if matches is None:
        return None;
    
    min_count, max_count, letter, password = matches.groups()
    return PasswordEntry(password, PasswordPolicy(letter, int(min_count), int(max_count)))

def test_password(password, policy):
    count = len([x for x in password if x == policy.letter])
    return policy.min_count <= count <= policy.max_count

if __name__ == '__main__':
    input = [parse_password_entry(x) for x in read_input().split("\n") if x]
    valid_passwords = [x for x in input if test_password(x.password, x.policy)]
    print(f"{len(valid_passwords)} out of {len(input)} passwords are valid.")
