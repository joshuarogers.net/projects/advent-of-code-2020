import re

def read_input():
    with open('input.txt') as file:
        return file.read()

class PasswordEntry:
    def __init__(self, password, policy):
        self.password = password
        self.policy = policy

class PasswordPolicy:
    def __init__(self, letter, position1, position2):
        self.letter = letter
        self.position1 = position1
        self.position2 = position2

def parse_password_entry(row):
    matches = re.search("^(\d+)-(\d+) (\w): (\w+)$", row)
    if matches is None:
        return None;
    
    min_count, max_count, letter, password = matches.groups()
    return PasswordEntry(password, PasswordPolicy(letter, int(min_count) - 1, int(max_count) - 1))

def test_password(password, policy):
    position1 = password[policy.position1]
    position2 = password[policy.position2]
    
    if position1 == position2:
        return False
        
    return position1 == policy.letter or position2 == policy.letter 

if __name__ == '__main__':
    input = [parse_password_entry(x) for x in read_input().split("\n") if x]
    valid_passwords = [x for x in input if test_password(x.password, x.policy)]
    print(f"{len(valid_passwords)} out of {len(input)} passwords are valid.")
