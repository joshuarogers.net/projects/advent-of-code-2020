def read_input():
    with open('input.txt') as file:
        return file.read()

def is_tree(tree_map, x, y):
    x %= len(tree_map[y])
    return tree_map[y][x]

if __name__ == '__main__':
    dx = 3
    dy = 1

    detect_tree = lambda x: True if x == '#' else False
    tree_map = [[detect_tree(y) for y in x] for x in read_input().split()]
    path_tree_vector = [is_tree(tree_map, x * dx, x) for x in range(0, len(tree_map))]
    tree_count = len([x for x in path_tree_vector if x])
    print(f"Passed {tree_count} trees")