import functools

def read_input():
    with open('input.txt') as file:
        return file.read()

def is_tree(tree_map, x, y):
    x %= len(tree_map[y])
    return tree_map[y][x]

def count_trees_in_vector(tree_map, dx, dy):
    path_test_coordinates_count = int(len(tree_map) / dy)
    path_test_coordinates = [(x * dx, x * dy) for x in range(0, path_test_coordinates_count)]
    path_tree_vector = [is_tree(tree_map, x, y) for (x, y) in path_test_coordinates]
    tree_count = len([x for x in path_tree_vector if x])
    return tree_count

if __name__ == '__main__':
    vectors = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]

    detect_tree = lambda x: True if x == '#' else False
    tree_map = [[detect_tree(y) for y in x] for x in read_input().split()]

    path_vector_tree_counts = [count_trees_in_vector(tree_map, dx, dy) for (dx, dy) in vectors]
    vector_product = functools.reduce(lambda x, y: x * y, path_vector_tree_counts)
    print(f"Vector product: {vector_product}")