def read_input():
    with open('input.txt') as file:
        return file.read()

def is_valid_passport(text):
    required_fields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']
    return all([f"{x}:" in text for x in required_fields])

if __name__ == '__main__':
    raw_lines = read_input().split('\n\n')
    valid_passports = [x for x in raw_lines if is_valid_passport(x)]
    print(f"{len(valid_passports)} of {len(raw_lines)} were valid")
