from enum import Enum

def read_input():
    with open('input.txt') as file:
        return file.read().strip().split('\n')

def step_seat_simulation(room):
    neighbors = [(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)]
    
    def get_seat(x, y):
        return room[y][x] if 0 <= y < len(room) and 0 <= x < len(room[y]) else None
    
    def simulate_seat(x, y):
        if get_seat(x, y) == SeatStatus.FLOOR:
            return SeatStatus.FLOOR
            
        occupied_neighbor_count = len([x for x in [get_seat(x + dx, y + dy) for dx, dy in neighbors] if x == SeatStatus.OCCUPIED])
        
        if occupied_neighbor_count == 0:
            return SeatStatus.OCCUPIED
        if occupied_neighbor_count >= 4:
            return SeatStatus.UNOCCUPIED
        return get_seat(x, y)

    return [[simulate_seat(x, y) for x in range(0, len(room[y]))] for y in range(0, len(room))]

class SeatStatus(Enum):
    FLOOR = 1
    UNOCCUPIED = 2
    OCCUPIED = 3
                                                
if __name__ == '__main__':
    seat_status_mapping = { '.': SeatStatus.FLOOR, 'L': SeatStatus.UNOCCUPIED, '#': SeatStatus.OCCUPIED }
    initial_room_layout = [[seat_status_mapping[x] for x in row] for row in read_input()]
    
    this_step = initial_room_layout
    prev_step = [[]]
    
    while [x for y in prev_step for x in y] != [x for y in this_step for x in y]:
        prev_step = this_step
        this_step = step_seat_simulation(this_step)

    occupied_seat_count = len([x for y in this_step for x in y if x == SeatStatus.OCCUPIED])
    print(f"{occupied_seat_count} seats end up occupied")
