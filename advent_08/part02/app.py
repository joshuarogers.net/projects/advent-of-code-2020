def read_input():
    with open('input.txt') as file:
        return file.read().strip().split('\n')

class Processor:
    def __init__(self, opcodes):        
        self.opcodes = opcodes
        self.acc = 0
        self.program_counter = 0
        
    def tick(self):
        asm, arg = self.opcodes[self.program_counter]

        if asm == 'acc':
            self.acc += arg
            
        offset = arg if asm == 'jmp' else 1
        self.program_counter += offset

def does_program_terminate(opcodes):
    processor = Processor(opcodes)
    processed_addresses = set([])
    while processor.program_counter not in processed_addresses and processor.program_counter < len(opcodes):
        processed_addresses.add(processor.program_counter)
        processor.tick()
    return processor.program_counter == len(opcodes)

def patch_opcode(opcodes, index):
    replacements = {
        'nop': 'jmp',
        'jmp': 'nop',
        'acc': 'acc'
    }
    
    patched_opcodes = opcodes + []
    asm, arg = patched_opcodes[index]
    patched_opcodes[index] = (replacements[asm], arg)
    return patched_opcodes

if __name__ == '__main__':
    opcodes = [(asm, int(arg)) for asm, arg in [x.split(' ') for x in read_input()]]
    patched_opcodes = [patch_opcode(opcodes, index) for index in range(0, len(opcodes))]
    
    for patched_opcode in patched_opcodes:
        if does_program_terminate(patched_opcode):
            processor = Processor(patched_opcode)
            while processor.program_counter != len(opcodes):
                processor.tick()
            print(f"Acc: {processor.acc}, PC: {processor.program_counter}")
