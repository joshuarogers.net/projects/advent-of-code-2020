def read_input():
    with open('input.txt') as file:
        return file.read().strip().split('\n')

if __name__ == '__main__':
    BUCKET_SIZE = 25
    
    numbers = [int(x) for x in read_input()]
    for x in range(BUCKET_SIZE, len(numbers) - 1):
        # The splice x-BUCKET_SIZE:x (exclusive) represents the potential values that can be
        # added together and x represents the value the should be reached by two of the addends.
        current_test_sum = numbers[x]
        potential_addends = numbers[x - BUCKET_SIZE : x]
        matched_addends = [x for x in potential_addends if current_test_sum - x in potential_addends]
        if len(matched_addends) < 2:
            print(f"No addends were found for {current_test_sum}.")
